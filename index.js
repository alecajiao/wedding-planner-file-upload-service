const {Storage} = require('@google-cloud/storage');
const storage = new Storage();
const cors = require('cors')(); 

exports.uploadReceipt = async (req, res) => {
    cors (req,res, async () => {
        const body = req.body;
        const bucket = storage.bucket('wedding-planner-receipt-bucket'); 
        const buffer = Buffer.from(body.img64, "base64");
        const file = bucket.file(`${body.imgName}`);

        // side effects
        await file.save(buffer); 
        await file.makePublic();

        res.send({ photoLink: file.publicUrl() });
    });
};
